package urbanizacja;

class Village extends Locality
{

// Atrybuty:
  private  static Integer StaticAttribute=32;

// Getters & Setters:
  private Integer getStaticAttribute() { return StaticAttribute; }

// Operacje:
  public  static  Integer StaticMethod () {
    return StaticAttribute*5;
  }

};
